move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_second_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

   
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_second_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_second_wall))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_second_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_second_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;


//gruound collision
if(fearofheight && place_meeting(x,y+8,obj_second_wall) &&
   !position_meeting(x+(sprite_width/2)*dir,y+(sprite_height/2)+8,obj_second_wall))
{
     dir *= -1;
}
     
