gravity_direction = 0.2;
hsp = 0;
vsp = 0;
grav = 0.2;
movespeed = 4;
mirar = false;

jumpspeed_normal = 6;
jumpspeed_powerup = 10;
jumpspeed = jumpspeed_normal;
obj_player.invensibility = false;

//Constants
grounded = false;
jumping = false;
fearofheight = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 1;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;
key_down = 0;
dir = -1;
